#include <stdlib.h>

void *malloc(size_t size) {
  void *mem = sys_mmap(0, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0); // FIXME
  return mem;
}

void free(void *ptr) {
  sys_munmap(ptr, sizeof(ptr));
}

_Noreturn void exit(int status) {
  sys_exit(status);
}

int abs(int i) {
  return i > 0 ? i : -i;
}
