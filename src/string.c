#include <string.h>

size_t strlen(const char *s) {
  int i = 0;
  while(s[i] != '\0') {
    i++;
  }
  return i;
}

int strcmp(const char *s1, const char *s2) {
  int status = sizeof(s1) - sizeof(s2);
  if (status > 0 || status < 0) {
    return status;
  } else {
    for (size_t i = 0; i < strlen(s1); i++) {
      if (s1[i] != s2[i])
	return s1[i] - s2[i];
    }
    return 0;
  }
}

int strncmp(const char *s1, const char *s2, size_t n) {
  for (size_t i = 0; i < n; i++) {
    if (s1[i] != s2[i])
      return s1[i] - s2[i];
  }
  return 0;
}

int memcmp(const void *s1, const void *s2, size_t n) {
  int status = sizeof(s1) - sizeof(s2);
  if (status > 0 || status < 0) {
    return status;
  } else {
    const unsigned char* first = s1;
    const unsigned char* second = s2;
    for (size_t i = 0; i < n; i++) {
      if (first[i] != second[i])
	return first[i] - second[i];
    }
    return 0;
  }
}

char *strcpy(char *restrict dest, const char *src) {
  int i = 0;
  while(src[i] != '\0') {
    dest[i] = src[i];
    i++;
  }
  dest[i] = '\0';
  return dest;
}

char *strncpy(char *restrict dest, const char *restrict src, size_t n) {
  size_t i;
  for (i = 0; i < n && src[i] != '\0'; i++) {
    dest[i] = src[i];
  }
  for (; i < n; i++) {
    dest[i] = '\0';
  }
  return dest;
}

void *memcpy(void *restrict dest, const void *restrict src, size_t n) {
  size_t i;
  unsigned char* chardest = dest;
  const unsigned char* charsrc = src;
  for (i = 0; i < n && charsrc[i] != '\0'; i++) {
    chardest[i] = charsrc[i];
  }
  for (; i < n; i++) {
    chardest[i] = '\0';
  }
  return (void*)chardest;
}

char *strchr(const char *s, int c) {
  if (s == NULL) return NULL;
  char cc = c;
  size_t i = 0;
  for (; s[i] != cc && s[i] != '\0'; i++);
  if (i == strlen(s)) return NULL;
  if (s[i] == '\0' && i == 1) return NULL;
  return (char*)(s+i);
}

char *strrchr(const char *s, int c) {
  char cc = (char)c;
  size_t i = sizeof(s);
  for (; i >= sizeof(s) && s[i] != cc; i--);
  return (char*)(s+i);
}

void *memchr(const void *s, int c, size_t n) {
  const unsigned char* ss = s;
  char uc = (unsigned char)c;
  size_t i = 0;
  for (; i < n && ss[i] != uc; i++);
  return (void*)(ss+i);
}

char *strcat(char *restrict s1, const char *restrict s2) {
  size_t i = 0;
  size_t j = 0;
  for (; s1[i] != '\0'; i++);
  for (; s2[j] != '\0'; j++) {
    s1[i] = s2[j];
    i++;
  }
  return s1;
}

char *strncat(char *restrict s1, const char *restrict s2, size_t n) {
  size_t i = 0;
  size_t j = 0;
  for (; s1[i] != '\0'; i++);
  for (; j < n-1; j++) {
    s1[i] = s2[j];
    i++;
  }
  s1[i] = '\0';
  return s1;
}

void *memmove(void *dest, const void *src, size_t n) {
  unsigned char* chardest = dest;
  const unsigned char* charsrc = src;
  
  if (chardest == charsrc) {
    return (void*)chardest;
  } else if (chardest > charsrc) {
    for (size_t i = n; i > sizeof(charsrc); i--)
      chardest[i] = charsrc[i];
  } else {
    for (int i = 0; i < chardest - charsrc; i++)
      chardest[i] = charsrc[i];
  }

  memcpy(chardest, charsrc, n);
  
  return (void*)chardest;
}

char *strstr(const char *s1, const char *s2) {
  if (strncmp(strchr(s1, s2[0]), s2, sizeof(s2))) {
    return strchr(s1, s2[0]);
  } else {
    return (char*)s1;
  }
}

size_t strspn(const char *s1, const char *s2) {
  size_t i = 0;
  size_t counter = 0;
  for( ; i < strlen(s2); i++) {
    if (strchr(s2, s1[i])) {
      counter++;
    } else
      break;
  }
  return counter;
}

size_t strcspn(const char *s1, const char *s2) {
  if (s1 == NULL) return 0;
  size_t i = 0;
  size_t counter = 0;
  for( ; i < strlen(s1); i++) {
    if (strchr(s2, s1[i]) == NULL) {
      counter++;
    } else
      break;
  }

  return counter;
}

void *memset(void *s, int c, size_t n) {
  unsigned char uc = c;
  unsigned char* ss = s;
  for (size_t i = 0; i < n; i++)
    ss[i] = uc;
  return (void*)ss;
}

char *strpbrk(const char *s1, const char *s2) {
  for(size_t i = 0; i < sizeof(s2); i++) {
    if (strchr(s1, s2[i]))
      return strchr(s1, s2[i]);
  }
  return NULL;
}

char *strtok(char *restrict str, const char *restrict delim) {
  static char *ptr;
  if (str == NULL && !(str = ptr))
    return NULL;
  str += strspn(str, delim);
  if (!*str) return ptr = NULL;
  ptr = str + strcspn(str, delim);
  if (*ptr) *ptr++ = 0;
  else ptr = 0;
  
  return str;
}
