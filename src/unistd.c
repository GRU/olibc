#include <unistd.h>
#include <liblinux.h>

ssize_t read(int fildes, void *buf, size_t nbyte) {
  return sys_read(fildes, buf, nbyte);
}

ssize_t write(int fildes, const void *buf, size_t nbyte) {
  return sys_write(fildes, buf, nbyte);
}

int close(int fildes) {
  return sys_close(fildes);
}
