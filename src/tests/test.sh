#!/bin/sh

# Simple script for testing olibc
# Recommended to execute in directory tests/ or src/

CC=cc

cd ../build/tests

if [[ -z $@ ]]
then 
    headerslist=$(ls)
else
    headerslist=$@
fi

if [ "$1" == "--help" ] || [ "$1" == "-h" ]
then
   echo "Simple utility for testing olibc"
   echo "- To run a test for some headers run this script with header names as argument (for example: ./test.sh string)"
   echo "- To test all headers just run this script without arguments"
   echo "Options:"
   echo "        -h | --help - display this message "
   echo "        -lh | --list-headers - display all headers that have tests"
   echo "        -lf | --list-functions - display all functions that have tests from header"
   exit
elif [ "$1" == "--list-headers" ] || [ "$1" == "-lh" ]
then
    for i in $(ls); do
	if [ -d $i ]
	then
	   echo $i
	fi
    done
    exit
elif [ "$1" == "--list-functions" ] || [ "$1" == "-lf" ]
then
    for i in $headerslist; do
	if [ -d $i ]
	then
	    cd $i
	    for j in $(ls); do
		if [ -x $j ] && [[ $j != *"-cc"* ]]
		then
		    echo "$i/$j"
		fi
	    done
	else
	    if [[ $i != "-"* ]]
	    then
		echo "Tests for $i not found."
	    fi
        fi
    done
    exit
elif [[ "$1" == "-"* ]]
then
    echo "No such flag: $1, check --help for help"
    exit
fi

for i in $headerslist; do
    if [ -d $i ]
    then
	echo ------------------------
	echo --- TESTING $i.h ---
	echo ------------------------
	echo 
	cd $i
	for j in $(ls); do
	    if [ -x $j ] && [[ $j != *"-cc"* ]]
	    then
		cat $(echo ../../../src/tests/$i/$j | sed 's/\.o/\.c/g') | cc -x c -o $j-cc -
	        ./$j > tmp
		./$j-cc > tmp-cc
		if [ "$(diff tmp tmp-cc)" == "" ]
		then
		    echo $j - TEST SUCCESS
		else
		    echo $j - TEST FAILED
		    echo "---- DIFF OUTPUT ----"
		    diff -y tmp tmp-cc
		fi
		echo "---"
	    fi
	done
    else
	echo "Tests for $i not found."
    fi
done


