// This is test program for strstr function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s[] = "Hi.Hello.Hey!";
  puts(strstr(s, "."));
  return 0;
}
