// This is test program for strncpy function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "FIRSTT";
  char s2[] = "SECOND";
  puts(strncpy(s1, s2, 4));
  return 0;
}
