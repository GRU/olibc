// This is test program for strchr function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s[] = "Hi.Hello.Hey!";
  puts(strchr(s, '.'));
  return 0;
}
