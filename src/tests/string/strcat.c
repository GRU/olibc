// This is test program for strcat function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "Hello,";
  char s2[] = " World!";
  puts(strcat(s1, s2));
  return 0;
}
