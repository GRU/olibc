// This is test program for memcpy function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "FIRSTT";
  char s2[] = "SECOND";
  puts(memcpy(s1, s2, 4));
  return 0;
}
