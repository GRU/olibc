// This is test program for strtok function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s[] = "Hi.Hey.Hello";
  puts(strtok(s, "."));
  puts(strtok(NULL, "."));
  return 0;
}
