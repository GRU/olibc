// This is test program for strlen function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s[] = "This is test string";
  printf("%d\n", strlen(s));
  return 0;
}
