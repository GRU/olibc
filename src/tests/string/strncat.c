// This is test program for strncat function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "Hello,";
  char s2[] = " World! this should be in output";
  puts(strncat(s1, s2, 7));
  return 0;
}
