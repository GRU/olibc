// This is test program for memcmp function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "This is test string";
  char s2[] = "This is second test string";
  printf("%d\n", memcmp(s1, s2, strlen(s1)));
  printf("%d\n", memcmp(s2, s1, strlen(s1)));
  return 0;
}
