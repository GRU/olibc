// This is test program for strrchr function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s[] = "Hi.Hello.Hey!";
  puts(strrchr(s, '.'));
  return 0;
}
