// This is test program for strspn function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "HELLO, this won't be counted";
  char s2[] = "EHLOL";
  printf("%d\n", strspn(s1, s2));
  return 0;
}
