// This is test program for strcpy function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "FIRSTT";
  char s2[] = "SECOND";
  puts(strcpy(s1, s2));
  return 0;
}
