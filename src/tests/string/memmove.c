// This is test program for memmove function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "FIRSTT";
  char s2[] = "SECOND";
  puts(memmove(s1, s2, 4));
  return 0;
}
