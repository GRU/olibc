// This is test program for memchr function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s[] = "Hi.Hello.Hey!";
  puts(memchr(s, '.', 9));
  return 0;
}
