// This is test program for strpbrk function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s[] = "Hello, World!";
  puts(strpbrk(s, "W"));
  return 0;
}
