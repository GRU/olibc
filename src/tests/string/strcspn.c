// This is test program for strcspn function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "HELLO, this won't be counted";
  char s2[] = "ihst";
  printf("%d\n", strcspn(s1, s2));
  return 0;
}
