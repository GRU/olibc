// This is test program for strcmp function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s1[] = "This is test string";
  char s2[] = "This is second test string";
  printf("%d\n", strcmp(s1, s2));
  printf("%d\n", strcmp(s2, s1));
  return 0;
}
