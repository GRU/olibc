// This is test program for memset function from string.h

#include <string.h>
#include <stdio.h>

int main() {
  char s[] = "TEST HELLO";
  puts(memset(s, '$', 4));
  return 0;
}
