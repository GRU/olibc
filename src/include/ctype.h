#ifndef _CTYPE_H
#define _CTYPE_H

int isdigit(int c);
int isxdigit(int c);
int isblank(int c);
int isspace(int c);
int islower(int c);
int isupper(int c);
int isalpha(int c);
int isalnum(int c);
int iscntrl(int c);
int isprint(int c);
int isgraph(int c);
int tolower(int c);
int toupper(int c);

#endif
