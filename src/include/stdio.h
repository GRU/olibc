#ifndef _STDIO_H
#define _STDIO_H

#include <liblinux.h>
#include <stdarg.h>
#include <sys/stat.h>

#include <stdlib.h>

#define EOF (-1)

FILE* fopen(const char *restrict pathname, const char *restrict mode);
int fclose(FILE *stream);
int fgetc(FILE *stream);
#define getc(f) fgetc(f)
int getchar(void);
char *fgets(char *restrict s, int n, FILE *restrict stream);

int fputc(int c, FILE *stream);
#define putc(c, f) fputc(c, f)
int fputs(const char *restrict s, FILE *restrict stream);

int rename(const char *old, const char *new);

int putchar(int c);
int puts(const char *s);
int vsnprintf(char *restrict str, size_t size, const char *restrict format, va_list ap);
int snprintf(char *restrict str, size_t size, const char *restrict format, ...);
int vsprintf(char *restrict str, const char *restrict format, va_list ap);
int sprintf(char *restrict str, const char *restrict format, ...);
int vfprintf(FILE *restrict stream, const char *restrict format, va_list ap);
int fprintf(FILE *restrict stream, const char *restrict format, ...);
int vprintf(const char *restrict format, va_list ap);
int printf(const char *restrict format, ...);

#endif
