#ifndef _STRING_H
#define _STRING_H

#include <stddef.h>

size_t strlen(const char *s);
int strcmp(const char *s1, const char *s2);
int strncmp(const char *s1, const char *s2, size_t n);
int memcmp(const void *s1, const void *s2, size_t n);
char *strcpy(char *restrict dest, const char *src);
char *strncpy(char *restrict dest, const char *restrict src, size_t n);
void *memcpy(void *restrict dest, const void *restrict src, size_t n);
char *strchr(const char *s, int c);
void *memchr(const void *s, int c, size_t n);
char *strrchr(const char *s, int c);
char *strcat(char *restrict s1, const char *restrict s2);
char *strncat(char *restrict s1, const char *restrict s2, size_t n);
void *memmove(void *dest, const void *src, size_t n);
char *strstr(const char *s1, const char *s2);
size_t strspn(const char *s1, const char *s2);
size_t strcspn(const char *s1, const char *s2);
void *memset(void *s, int c, size_t n);
char *strpbrk(const char *s1, const char *s2);
char *strtok(char *restrict str, const char *restrict delim);

#endif
