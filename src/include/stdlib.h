#ifndef _STDLIB_H
#define _STDLIB_H

#include <sys/mman.h>
#include <liblinux.h>
#include <stddef.h>

#define MAP_ANONYMOUS 0x20

void *malloc(size_t size);
void free(void *ptr);

_Noreturn void exit(int status);

int abs(int j);

#endif
